package oldfashionpoundrest.bean;

public abstract class OperationBean implements IOperationBean {
	private PriceBean firstValue;

	public PriceBean getFirstValue() {
		return firstValue;
	}

	public void setFirstValue(PriceBean firstValue) {
		this.firstValue = firstValue;
	}
}
