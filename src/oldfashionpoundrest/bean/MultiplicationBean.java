package oldfashionpoundrest.bean;

public class MultiplicationBean extends OperationBean {
	private int secondValue;

	public int getSecondValue() {
		return secondValue;
	}

	public void setSecondValue(int secondValue) {
		this.secondValue = secondValue;
	}
}
