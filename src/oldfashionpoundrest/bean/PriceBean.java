package oldfashionpoundrest.bean;

import oldfashionpound.Constants;

public class PriceBean {
	private int pence;
	private int scellino;
	private int pound;
	private int mod;

	public int getPence() {
		return pence;
	}
	public void setPence(int pence) {
		this.pence = pence;
	}
	public int getScellino() {
		return scellino;
	}
	public void setScellino(int scellino) {
		this.scellino = scellino;
	}
	public int getPound() {
		return pound;
	}
	public void setPound(int pound) {
		this.pound = pound;
	}
	public int getMod() {
		return mod;
	}
	public void setMod(int mod) {
		this.mod = mod;
	}

	@Override
	public String toString() {
		return new StringBuilder()
		.append(pound).append(String.valueOf(Constants.POUND)).append(" ")
		.append(scellino).append(String.valueOf(Constants.SCELLINO)).append(" ")
		.append(pence).append(String.valueOf(Constants.PENCE))
		.append(mod > 0 ? " - " + mod + Constants.MOD : "")
		.toString()
		;
	}
}
