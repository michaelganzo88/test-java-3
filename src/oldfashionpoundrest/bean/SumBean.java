package oldfashionpoundrest.bean;

public class SumBean extends OperationBean {
	private PriceBean secondValue;

	public PriceBean getSecondValue() {
		return secondValue;
	}
	public void setSecondValue(PriceBean secondValue) {
		this.secondValue = secondValue;
	}
}
