package oldfashionpoundrest.bean;

import oldfashionpoundrest.PriceBeanConverter;

public class OperationBeanFactory {
	private OperationBeanFactory() {}

	public static IOperationBean getOperationBean(Character mathOperator, String firstValue, String secondValue) {
		OperationBean operation = null;
		switch (mathOperator) {
		case '+':
			operation = new SumBean();
			SumBean operationSum = (SumBean) operation;
			operationSum.setSecondValue(new PriceBeanConverter(secondValue).getPriceBean());
			break;
		case '-':
			operation = new SubtractionBean();
			SubtractionBean operationSub = (SubtractionBean) operation;
			operationSub.setSecondValue(new PriceBeanConverter(secondValue).getPriceBean());
			break;
		case '*':
			operation = new MultiplicationBean();
			MultiplicationBean operationMul = (MultiplicationBean) operation;
			operationMul.setSecondValue(Integer.parseInt(secondValue));
			break;
		case '/':
			operation = new DivisionBean();
			DivisionBean operationDiv = (DivisionBean) operation;
			operationDiv.setSecondValue(Integer.parseInt(secondValue));
			break;
		default:
			break;
		}
		if(operation != null)
			operation.setFirstValue(new PriceBeanConverter(firstValue).getPriceBean());

		return operation;
	}
}
