package oldfashionpoundrest;

import java.util.regex.Pattern;

import oldfashionpoundrest.Constants;
import oldfashionpoundrest.bean.PriceBean;

public class PriceBeanConverter {
	String priceString;
	PriceBean priceBean;

	public PriceBeanConverter(String priceString) {
		this.priceString = priceString;
		convertToPriceBean();
	}
	public PriceBeanConverter(PriceBean priceBean) {
		this.priceBean = priceBean;
		convertToPriceString();
	}

	private void convertToPriceBean() {
		String regex = "^-?(\\d+" + Constants.POUND + ")-?(\\d+" + Constants.SCELLINO + ")-?(\\d+" + Constants.PENCE + ")$";
		if(Pattern.matches(regex, priceString)) {
			this.priceBean = new PriceBean();
			String[] psd = priceString.split("(?<=\\d\\D)");
			for(String unitPsd: psd) {
				setUnit(unitPsd);
			}
		}
	}

	private void convertToPriceString() {

	}

	private void setUnit(String unitPsd) {
		String[] unitAr = unitPsd.split("(?=\\D)");
		char unitId = unitAr[1].charAt(0);
		int unitValue = Integer.parseInt(unitAr[0]);
		switch (unitId) {
			case Constants.PENCE:
				this.priceBean.setPence(unitValue);
				break;
			case Constants.SCELLINO:
				this.priceBean.setScellino(unitValue);
				break;
			case Constants.POUND:
				this.priceBean.setPound(unitValue);
				break;
			default:
				break;
		}
	}

	public String getPriceString() {
		return priceString;
	}
	public void setPriceString(String priceString) {
		this.priceString = priceString;
	}
	public PriceBean getPriceBean() {
		return priceBean;
	}
	public void setPriceBean(PriceBean priceBean) {
		this.priceBean = priceBean;
	}
}
