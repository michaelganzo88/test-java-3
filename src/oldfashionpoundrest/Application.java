package oldfashionpoundrest;

import java.util.ArrayList;
import java.util.Arrays;

import com.fasterxml.jackson.databind.ObjectMapper;

import oldfashionpoundrest.bean.IOperationBean;
import oldfashionpoundrest.bean.OperationBeanFactory;
import oldfashionpoundrest.bean.ResultBean;
import oldfashionpoundrest.service.OperationServiceFactory;

public class Application {

	public static void main(String[] args) {

		String jsonRequestBody = "";
		try {
			String[] members = getArrayFromInput(args);
			if(members.length == 3) {
				IOperationBean operationBean = OperationBeanFactory.getOperationBean(members[1].charAt(0), members[0], members[2]);
				ObjectMapper mapper = new ObjectMapper();
				jsonRequestBody = mapper.writeValueAsString(operationBean);
				ResultBean response = OperationServiceFactory.getOperationResponse(jsonRequestBody, operationBean);
				System.out.println(response.getResult().toString());
			} else {
				throw new Exception("Formato input non corretto!");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	private static String[] getArrayFromInput(String[] args) {
		String[] members = null;
		ArrayList<String> test = new ArrayList<String>();
		String input = "";
		for(String arg: args) {
			input += arg.toLowerCase().trim();
			if(input.charAt(input.length()-1) == Constants.PENCE) {
				test.add(input);
				input = "";
			}
		}
		String[] secondArgAr;
		if(test.size() == 2) {
			String secondArg = test.get(1);
			secondArgAr = secondArg.split("(?<=[-+*/])|(?=[-+*/])");
			test.remove(1);
			test.addAll(Arrays.asList(secondArgAr));
		}
		members = new String[test.size()];
		return test.toArray(members);
	}
}
