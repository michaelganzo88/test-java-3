package oldfashionpoundrest.service;

import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;

public abstract class ClientService {

	private final static String urlStr = "http://localhost:8080";
	private URL url;
	HttpURLConnection conn;

	public ClientService(String path) throws Exception {
		this.url = new URL(urlStr + "/" + path);
		connect();
		setRequestMethod();
		setRequestProperty();
	}

	private void connect() throws Exception {
		this.conn = (HttpURLConnection) this.url.openConnection();
	}

	abstract void setRequestMethod() throws Exception;
	abstract void setRequestProperty();

	private void call() throws Exception {
		if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
			throw new RuntimeException("Failed : HTTP error code : " + conn.getResponseCode());
		}
	}

	public InputStream getResponse() throws Exception {
		call();
		return conn.getInputStream();
	}

	public void disconnect() {
		this.conn.disconnect();
	}

}
