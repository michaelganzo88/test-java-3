package oldfashionpoundrest.service;

import java.io.OutputStream;

public class ClientServicePost extends ClientService {

	private final String method = "POST";
	private final String contentType = "Content-Type";
	private final String applicationJson = "application/json";
	private String jsonRequestBody;

	public ClientServicePost(String path, String jsonRequestBody) throws Exception {
		super(path);
		this.jsonRequestBody = jsonRequestBody;
		setRequestBody();
	}

	@Override
	void setRequestMethod() throws Exception {
		this.conn.setDoOutput(true);
		this.conn.setRequestMethod(this.method);
	}

	@Override
	void setRequestProperty() {
		this.conn.setRequestProperty(contentType, applicationJson);
	}

	private void setRequestBody() throws Exception {
		OutputStream os = conn.getOutputStream();
		os.write(this.jsonRequestBody.getBytes());
		os.flush();
	}

}
