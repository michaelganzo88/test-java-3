package oldfashionpoundrest.service;

import com.fasterxml.jackson.databind.ObjectMapper;

import oldfashionpoundrest.bean.DivisionBean;
import oldfashionpoundrest.bean.IOperationBean;
import oldfashionpoundrest.bean.MultiplicationBean;
import oldfashionpoundrest.bean.ResultBean;
import oldfashionpoundrest.bean.SubtractionBean;
import oldfashionpoundrest.bean.SumBean;

public class OperationServiceFactory {
	private OperationServiceFactory() {}

	public static ResultBean getOperationResponse(String jsonRequestBody, IOperationBean operation) throws Exception {
		ResultBean result = null;
		String path = "";
		ClientService clientService = null;
		ObjectMapper mapper = new ObjectMapper();
		if(operation instanceof SumBean) {
			path = "somma";
		} else if(operation instanceof SubtractionBean) {
			path = "sottrazione";
		} else if(operation instanceof MultiplicationBean) {
			path = "moltiplicazione";
		} else if(operation instanceof DivisionBean) {
			path = "divisione";
		}
		clientService = new ClientServicePost(path, jsonRequestBody);
		if(clientService != null) {
			result = mapper.readValue(clientService.getResponse(), ResultBean.class);
			clientService.disconnect();
		}
		return result;
	}
}
