package oldfashionpound;

import java.util.HashMap;
import java.util.Map;

public final class Constants {
	public final static char PENCE = 'd';
	public final static char SCELLINO = 's';
	public final static char POUND = 'p';
	public final static char MOD = 'a';												// Math Division Module
	private final static int dMultiplicationFactor = 1;								// Pence
	private final static int sMultiplicationFactor = 12 * dMultiplicationFactor;	// Scellino
	private final static int pMultiplicationFactor = 20 * sMultiplicationFactor;	// Pound
	public final static Map<Character, Integer> pennyValues = getMultiplicationFactors();

	private static Map<Character, Integer> getMultiplicationFactors() {
		Map<Character, Integer> pennyValues = new HashMap<Character, Integer>();
		pennyValues.put(Constants.POUND, pMultiplicationFactor);
		pennyValues.put(Constants.SCELLINO, sMultiplicationFactor);
		pennyValues.put(Constants.PENCE, dMultiplicationFactor);
		return pennyValues;
	}
}
