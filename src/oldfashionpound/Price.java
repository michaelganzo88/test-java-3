package oldfashionpound;

import java.util.regex.Pattern;

public class Price {
	private int pounds;
	private int scellini;
	private int pence;
	private int totalPence;
	private int mod;
	
	public Price(String price) {
		String regex = "^(\\d+" + Constants.POUND + ")(\\d+" + Constants.SCELLINO + ")(\\d+" + Constants.PENCE + ")$";
		if(Pattern.matches(regex, price)) {
			String[] psp = price.split("(?<=\\d\\D)");
			for(String unitPsp: psp) {
				String[] unitAr = unitPsp.split("(?=\\D)");
				this.totalPence += Integer.parseInt(unitAr[0]) * Constants.pennyValues.get(unitAr[1].charAt(0));
			}
		}
		penceToPrice();
	}

	public Price(int pennies) {
		this.totalPence = pennies;
		penceToPrice();
	}
	
	private void penceToPrice() {
		int pounds = (totalPence / Constants.pennyValues.get(Constants.POUND));
		int scellini = (totalPence % Constants.pennyValues.get(Constants.POUND))/Constants.pennyValues.get(Constants.SCELLINO);
		int pence = (totalPence % Constants.pennyValues.get(Constants.SCELLINO));
		this.pounds = pounds;
		this.scellini = scellini;
		this.pence = pence;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(pounds).append(String.valueOf(Constants.POUND)).append(" ")
		.append(scellini).append(String.valueOf(Constants.SCELLINO)).append(" ")
		.append(pence).append(String.valueOf(Constants.PENCE))
		.append(mod > 0 ? " - " + mod + Constants.MOD : "")
		;
		return sb.toString();
	}
	
	public int getPennies() {
		return this.totalPence;
	}
	
	public void setMod(int mod) {
		this.mod = mod;
	}
}
