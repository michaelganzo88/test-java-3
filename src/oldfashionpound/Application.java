package oldfashionpound;

public class Application {

	public static void main(String[] args) {
		
		String input = "";
		try {
			for(String arg: args) {
				input += arg.toLowerCase().trim();
			}
			
			String[] mathCalcAr = input.split("(?<=[-+*/])|(?=[-+*/])");
			if(mathCalcAr != null && mathCalcAr.length == 3) {
				String result = Utils.doCalc(mathCalcAr[0], mathCalcAr[2], mathCalcAr[1]);
				System.out.println(result);
			} else {
				throw new Exception("Formato input non corretto!");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
