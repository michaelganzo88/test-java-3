package oldfashionpound;

public class Utils {
	
	public static String doCalc(String price1, String price2, String operator) {
		Price price = null;
		if(operator.equals("+")) {
			price = new Price(sum(new Price(price1), new Price(price2)));
		} else if(operator.equals("-")) {
			price = new Price(diff(new Price(price1), new Price(price2)));
		} else if(operator.equals("*")) {
			price = new Price(multiplication(new Price(price1), Integer.parseInt(price2)));
		} else if(operator.equals("/")) {
			Price oldPrice = new Price(price1);
			price = new Price(division(oldPrice, Integer.parseInt(price2)));
			Price tmp = new Price(price.getPennies() * Integer.parseInt(price2));
			price.setMod(oldPrice.getPennies() - tmp.getPennies());
		}
		return price.toString();
	}
	
	private static int sum(Price price1, Price price2) {
		return price1.getPennies() + price2.getPennies();
	}

	private static int diff(Price price1, Price price2) {
		return price1.getPennies() - price2.getPennies();
	}

	private static int multiplication(Price price, int multiplicationFactor) {
		return price.getPennies() * multiplicationFactor;
	}

	private static int division(Price price, int multiplicator) {
		return price.getPennies() / multiplicator;
	}
}
