package oldfashionpoundnew.unit;

import oldfashionpoundnew.Constants;

public class UnitPound extends UnitPrice {

	public UnitPound(int value) {
		super(value);
	}
	
	@Override
	void setIdentifier() {
		this.identifier = Constants.POUND;
	}
	
	@Override
	void setMultiplicator() {
		this.multiplicator = Constants.pMultiplicationFactor * Constants.sMultiplicationFactor;
	}

	@Override
	void setMaxValue() {
		this.maxValue = Integer.MAX_VALUE;
	}

}
