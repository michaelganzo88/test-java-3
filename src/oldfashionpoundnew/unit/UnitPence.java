package oldfashionpoundnew.unit;

import oldfashionpoundnew.Constants;

public class UnitPence extends UnitPrice {
	
	public UnitPence(int value) {
		super(value);
	}
	
	@Override
	void setIdentifier() {
		this.identifier = Constants.PENCE;
	}
	
	@Override
	void setMultiplicator() {
		this.multiplicator = Constants.dMultiplicationFactor;
	}

	@Override
	void setMaxValue() {
		this.maxValue = 12;
	}

}
