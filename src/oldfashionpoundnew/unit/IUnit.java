package oldfashionpoundnew.unit;

public interface IUnit {
	int getValue();
	int getPennies();
}
