package oldfashionpoundnew.unit;

import oldfashionpoundnew.Constants;

public class UnitScellino extends UnitPrice {
	
	public UnitScellino(int value) {
		super(value);
	}

	@Override
	void setIdentifier() {
		this.identifier = Constants.SCELLINO;
	}
	
	@Override
	void setMultiplicator() {
		this.multiplicator = Constants.sMultiplicationFactor * Constants.dMultiplicationFactor;
	}

	@Override
	void setMaxValue() {
		this.maxValue = 20;
	}

}
