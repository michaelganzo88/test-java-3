package oldfashionpoundnew.unit;

public abstract class UnitPrice implements IUnit {
	protected char identifier;
	protected int multiplicator;
	protected int pennies;
	protected int maxValue;
	protected int value;
	
	public UnitPrice(int value) {
		this.value = value;
		setIdentifier();
		setMultiplicator();
		setMaxValue();
		setPennies(value);
	}
	
	abstract void setIdentifier();
	abstract void setMultiplicator();
	abstract void setMaxValue();
	
	private void setPennies(int value) {
		this.pennies = value * this.multiplicator;
	}

	public char getIdentifier() {
		return identifier;
	}
	
	public int getMultiplicator() {
		return multiplicator;
	}
	
	public int getValue() {
		return value;
	}
	
	public int getPennies() {
		return pennies;
	}
	
	public int getMaxValue() {
		return maxValue;
	}
}
