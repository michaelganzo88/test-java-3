package oldfashionpoundnew;

public final class Constants {
	public final static char PENCE = 'd';
	public final static char SCELLINO = 's';
	public final static char POUND = 'p';
	public final static char MOD = 'a';					// Math Division Module
	public final static int dMultiplicationFactor = 1;	// Pence
	public final static int sMultiplicationFactor = 12;	// Scellino
	public final static int pMultiplicationFactor = 20;	// Pound
	
//	public final static Map<Character, Integer> pennyValues = getMultiplicationFactors();
//
//	private static Map<Character, Integer> getMultiplicationFactors() {
//		Map<Character, Integer> pennyValues = new LinkedHashMap<Character, Integer>();
//		pennyValues.put(Constants.PENCE, dMultiplicationFactor);
//		pennyValues.put(Constants.SCELLINO, sMultiplicationFactor);
//		pennyValues.put(Constants.POUND, pMultiplicationFactor);
//		return pennyValues;
//	}
}
