package oldfashionpoundnew;

import oldfashionpoundnew.math.IMathOperation;
import oldfashionpoundnew.math.MathOperationFactory;

public class ApplicationNew {

	public static void main(String[] args) {
		String input = "";
		try {
			for(String arg: args) {
				input += arg.toLowerCase().trim();
			}

			String[] mathCalcAr = input.split("(?<=[-+*/])|(?=[-+*/])");
			if(mathCalcAr != null && mathCalcAr.length == 3) {
				IMathOperation mathOperation = MathOperationFactory.getMathOperation(mathCalcAr[0], mathCalcAr[2], mathCalcAr[1].charAt(0));
				if(mathOperation != null) {
					System.out.println(mathOperation.getResultPrice().toString());
				}
			} else {
				throw new Exception("Formato input non corretto!");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

}
