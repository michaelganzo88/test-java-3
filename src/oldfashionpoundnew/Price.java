package oldfashionpoundnew;

import java.util.regex.Pattern;

import oldfashionpoundnew.unit.IUnit;
import oldfashionpoundnew.unit.UnitFactory;
import oldfashionpoundnew.unit.UnitPence;
import oldfashionpoundnew.unit.UnitPound;
import oldfashionpoundnew.unit.UnitScellino;

public class Price {
	private UnitPound pounds;
	private UnitScellino scellini;
	private UnitPence pence;
	private UnitPence unitPence;
	private int mod;

	public Price(int pennies) {
//		this.totalPence = pennies;
		this.unitPence = new UnitPence(pennies);
		penceToPrice();
	}

	public Price(String price) throws Exception {
		parsePrice(price);
//		penceToPrice();
	}
	
	private void parsePrice(String price) throws Exception {
		String regex = "^(\\d+" + Constants.POUND + ")(\\d+" + Constants.SCELLINO + ")(\\d+" + Constants.PENCE + ")$";
		if(Pattern.matches(regex, price)) {
			String[] psp = price.split("(?<=\\d\\D)");
			int totalPence = 0;
			for(String unitPsp: psp) {
				IUnit unit = readUnit(unitPsp);
				setUnits(unit);
				totalPence += unit.getPennies();
			}
			setUnitPence(totalPence);
		}
	}
	
	private IUnit readUnit(String unitPsp) {
		String[] unitAr = unitPsp.split("(?=\\D)");
		return UnitFactory.getUnit(unitAr[1].charAt(0), Integer.parseInt(unitAr[0]));
	}
	
	private void setUnits(IUnit unit) {
		if(unit instanceof UnitPence) {
			this.pence = (UnitPence) unit;
		} else if(unit instanceof UnitScellino) {
			this.scellini = (UnitScellino) unit;
		} else if(unit instanceof UnitPound) {
			this.pounds = (UnitPound) unit;
		}
	}
	
	private void setUnitPence(int totalPence) {
		this.unitPence = new UnitPence(totalPence);
	}

	private void penceToPrice() {
		int sMultiplicator = new UnitScellino(0).getMultiplicator();
		int pMultiplicator = new UnitPound(0).getMultiplicator();
		this.pounds = new UnitPound(this.unitPence.getPennies() / pMultiplicator);
		this.scellini = new UnitScellino(this.unitPence.getPennies() % pMultiplicator/sMultiplicator);
		this.pence = new UnitPence(this.unitPence.getPennies() % sMultiplicator);
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append(pounds.getValue()).append(String.valueOf(Constants.POUND)).append(" ")
		.append(scellini.getValue()).append(String.valueOf(Constants.SCELLINO)).append(" ")
		.append(pence.getValue()).append(String.valueOf(Constants.PENCE))
		.append(mod > 0 ? " - " + mod + Constants.MOD : "")
		;
		return sb.toString();
	}

	public int getPennies() {
		return this.unitPence.getPennies();
	}

	public void setMod(int mod) {
		this.mod = mod;
	}
}
