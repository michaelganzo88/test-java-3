package oldfashionpoundnew.math;

import oldfashionpoundnew.Price;

public class Multiplication extends MathOperation {

	protected Multiplication(Price firstArg, int secondArg) {
		super(firstArg);
		this.secondArg = secondArg;
		calculate();
	}

	@Override
	void calculate() {
		this.risultato = this.firstArg * this.secondArg;
	}

}
