package oldfashionpoundnew.math;

import oldfashionpoundnew.Price;

public class Division extends Multiplication {

	protected Division(Price firstArg, int secondArg) {
		super(firstArg, secondArg);
	}

	@Override
	void calculate() {
		this.risultato = this.firstArg / this.secondArg;
	}

	@Override
	public Price getResultPrice() {
		Price firstPrice = new Price(this.firstArg);
		Price firstTmpPrice = new Price(risultato*secondArg);
		int diffPennies = firstPrice.getPennies() - firstTmpPrice.getPennies();
		Price resultPrice = new Price(risultato);
		resultPrice.setMod(diffPennies);
		return resultPrice;
	}

}
