package oldfashionpoundnew.math;

import oldfashionpoundnew.Price;

public interface IMathOperation {
	int getResult();
	Price getResultPrice();
}
