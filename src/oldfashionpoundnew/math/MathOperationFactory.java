package oldfashionpoundnew.math;

import oldfashionpoundnew.Price;

public class MathOperationFactory {

	public static IMathOperation getMathOperation(String firstArgStr, String secondArgStr, char mathOperator) throws Exception {
		IMathOperation mathOperation = null;
		Price firstArg = new Price(firstArgStr);
		switch (mathOperator) {
		case '+':
			mathOperation = new Sum(firstArg, new Price(secondArgStr));
			break;
		case '-':
			mathOperation = new Subtraction(firstArg, new Price(secondArgStr));
			break;
		case '*':
			mathOperation = new Multiplication(firstArg, Integer.parseInt(secondArgStr));
			break;
		case '/':
			mathOperation = new Division(firstArg, Integer.parseInt(secondArgStr));
			break;
		default:
			break;
		}
		return mathOperation;
	}

}
